/*
 * Copyright (C) 2011-2012 Project SkyFire <http://www.projectskyfire.org/>
 * Copyright (C) 2008-2012 TrinityCore <http://www.trinitycore.org/>
 * Copyright (C) 2005-2012 MaNGOS <http://getmangos.com/>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "ScriptPCH.h"
#include "deadmines.h"


enum Yells
{
	SAY_AGGRO_1 = ;
	SAY_AGGRO_2 = ;
	SAY_ARCAN_1 = ;
	SAY_ARCAN_2 = ;
	SAY_FIREWAL = ;
	SAY_FROST_F = ;
	SAY_FIRE_FI = ;
	SAY_DEATH_1 = ;
}

enum eSpels
{
	//D�clarations des spells avec mode h�ro//

	//phase1
    SPELL_FIST_OF_FLAME     = 87859,
	SPELL_FIST_OF_FROST     = 87861,
	SPELL_TRANSFER			= 119415,

	//phase2
	SPELL_ARCANE_POWER      = 88009,
    SPELL_FIRE_BLOSSOM      = 88173,
    SPELL_FROST_BLOSSOM     = 88169,
	SPELL_FIRE_WALL			= 91398,
};

enum Creatures
{
	NPC_FIRE_BLOSSOM = 48957,
	NPC_FROST_BLOSSOM = 48958,
}



class boss_glubtok : public CreatureScript
{
public:
    boss_glubtok() : CreatureScript("boss_glubtok") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_glubtokAI (creature);
    }

    struct boss_glubtokAI : public ScriptedAI
    {
        boss_glubtokAI(Creature* creature) : ScriptedAI(creature)
        {
            instance = creature->GetInstanceScript();
        }

        InstanceScript* instance;

		uint32 FistsFire;
		uint32 Phase;
       
		void Reset()
		{
			//� ajuster correctement et/ou rajouter, enlever// 
			Phase = 1;
			

		}

		void EnterCombat(Unit* who)
            {
                DoScriptText(SAY_AGGRO, me, who);
            }

		void UpdateAI(const uint32 uiDiff)
            {
                
                
                DoMeleeAttackIfReady();
            }
        };

	 
	CreatureAI* GetAI(Creature* creature) const
        {
            return new example_creatureAI(creature);
        }

    };
};

void AddSC_boss_glubtok()
{
    new boss_glubtok();
}