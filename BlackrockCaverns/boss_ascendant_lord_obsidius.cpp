/*
	Script fait pour le serveur priv� DragonWar.fr

	�tat du script : 95%
	Avancement : Reste seulement � test les ombres en HM

	N.B.: Ne d�tient aucun copyright, peut �tre modifi� sans aucune permission de l'auteur.
*/

#include "ScriptPCH.h"
#include "blackrock_caverns.h"


enum Spells
{
	// Sort du boss

	SPELL_TRANSFORM = 76196,
	SPELL_STONE_BLOW = 76185,
	SPELL_CORRUPTION = 76188,
	SPELL_THUNDERCLAP = 84622,

	//Sort des ombres : 2 NM, 3 HM

	SPELL_TWITCHY	= 76167,
	SPELL_CREPUSCULAR_VEIL = 76189,
}

enum Events
{
	EVENT_THUNDERCLAP = 1,
	EVENT_TWILIGHT_CORRUPTION = 2,
	EVENT_STONE_BLOW = 3,
}


Position const summonPositions[3] =
{
	{349.049f, 569.186f, 66.0078f, 3.11421f},
	{350.668f, 553.744f, 66.0078f, 3.12599f},
	{344.015f, 561.265f, 66.0078f, 3.15348f},
};

class boss_ascendant_lord_obsidius : public CreatureScript
{
public:
    boss_ascendant_lord_obsidius() : CreatureScript("boss_ascendant_lord_obsidius") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_ascendant_lord_obsidiusAI (creature);
    }

    struct boss_ascendant_lord_obsidiusAI : public ScriptedAI
    {
        boss_ascendant_lord_obsidiusAI(Creature* creature) : ScriptedAI(creature)
        {
            instance = creature->GetInstanceScript();
			for(uint8 i = 0; i <= RAID_MODE(1,2); i++)
				ShadowOfObsidiusList[i] = NULL;
        }

        InstanceScript* instance;

		Creature* ShadowOfObsidiusList[3];
		EventMap events;
		uint8 Phase;

        void Reset() 
		{
			events.Reset();

			me->GetMotionMaster()->MoveTargetedHome();

			for(uint8 i = 0; i <= RAID_MODE(1,2); i++)
			{
				if(ShadowOfObsidiusList[i] == NULL)
				ShadowOfObsidiusList[i] = me->SummonCreature(NPC_SHADOW_OF_OBSIDIUS,summonPositions[i],TEMPSUMMON_MANUAL_DESPAWN);

				if(ShadowOfObsidiusList[i]->isDead())
				{
					ShadowOfObsidiusList[i]->Respawn();
					ShadowOfObsidiusList[i]->GetMotionMaster()->MoveTargetedHome();
				}
			}
		}

        void EnterCombat(Unit* /*who*/) 
		{
			events.ScheduleEvent(EVENT_TWILIGHT_CORRUPTION, 10000);
			events.ScheduleEvent(EVENT_STONE_BLOW, 13000);

			if(me->GetMap()->IsHeroic())
			events.ScheduleEvent(EVENT_THUNDERCLAP, 7000);

			Phase = 0;

			for(uint8 i = 0; i <= RAID_MODE(1,2); i++)
				ShadowOfObsidiusList[i]->Attack(me->getVictim(),false);

			me->MonsterYell("Vous cherchez des r�ponses ! Alors voil� ma r�ponse � votre existence !", LANG_UNIVERSAL, NULL);
		}

        void UpdateAI(const uint32 Diff)
        {
			if (!UpdateVictim() || me->HasUnitState(UNIT_STAT_CASTING))
				return;

			if ((me->HealthBelowPct(69) && Phase == 0) || (me->HealthBelowPct(34) && Phase == 1))
			{
				Phase++;

				//Inverse la position du boss avec l'ombre

				Creature* target = ShadowOfObsidiusList[urand(0,RAID_MODE(1,2))];
				Position telePos;

				me->GetPosition(&telePos);

				// Inverse les positions en enlevant tout aggro
				me->NearTeleportTo(target->GetPositionX(),target->GetPositionY(),target->GetPositionZ(),0);
				target->NearTeleportTo(telePos.GetPositionX(),telePos.GetPositionY(),telePos.GetPositionZ(),0);
				me->getThreatManager().resetAllAggro();



				me->MonsterYell("Vous n'avez pas votre place dans le monde du ma�tre.", LANG_UNIVERSAL, NULL);

				return;
			}

			events.Update(diff);

			while (uint32 eventId = events.ExecuteEvent())
			{
				switch (eventId)
				{
				case EVENT_THUNDERCLAP:
					DoCastAOE(SPELL_THUNDERCLAP);
					events.ScheduleEvent(EVENT_THUNDERCLAP, 7000);
					break;

				case EVENT_TWILIGHT_CORRUPTION:
					if(Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0, 200, true))
					DoCast(pTarget,SPELL_TWILIGHT_CORRUPTION);

					events.ScheduleEvent(EVENT_TWILIGHT_CORRUPTION, 10000);
					break;

				case EVENT_STONE_BLOW:
					DoCastVictim(SPELL_STONE_BLOW);
					events.ScheduleEvent(EVENT_STONE_BLOW, 13000);
					break;

				default:
					break;
				}
			}

			void JustDied(Unit* /*killer*/)
			{
			for(uint8 i = 0; i <= RAID_MODE(1,2); i++)
			{
				ShadowOfObsidiusList[i]->DespawnOrUnsummon();

				ShadowOfObsidiusList[i] = NULL;
			}

			me->MonsterYell(" Personne ne peut me d�truire� Juste me� retar� der�", LANG_UNIVERSAL, NULL);
		}
            DoMeleeAttackIfReady();
        }
    };
};



void AddSC_boss_ascendant_lord_obsidius()
{
	
    new boss_ascendant_lord_obsidius();
}