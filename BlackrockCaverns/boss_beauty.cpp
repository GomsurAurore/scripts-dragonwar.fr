/*
	Script fait pour le serveur priv� DragonWar.fr

	�tat du script : 85%
	Avancement : Script du dernier chien (spot), test du boss, � AJOUTER � L'INSTANCE(absent)!!!

	N.B.: Ne d�tient aucun copyright, peut �tre modifi� sans aucune permission de l'auteur.
*/


#include "ScriptPCH.h"
#include "blackrock_caverns.h"

enum Spells
{
	//Spells Boss
	SPELL_BERSERKER_CHARGE = 56106,
	SPELL_FLAMEBREAK	= 76032,
	SPELL_TERRYFING_ROAR = 14100,
	SPELL_MAGMASPIT		= 76031,

	//Spells chiens

	SPELL_LAVA_DROOL	= 76628,
	SPELL_FLAME_BREATH  = 76665,

}

enum Events
{
	//Events Boss
	EVENT_BERSERKER = 1,
	EVENT_FLAMEBREAK = 2,
	EVENT_ROAR = 3,
	EVENT_MAGMASPIT = 4,

	//Events chiens
	EVENT_LAVA = 5,
	EVENT_BREATH = 6,

}

class boss_beauty : public CreatureScript
{
public:
    boss_beauty() : CreatureScript("boss_beauty") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_beautyAI (creature);
    }

    struct boss_beautyAI : public ScriptedAI
    {
        boss_beautyAI(Creature* creature) : ScriptedAI(creature)
        {
            instance = creature->GetInstanceScript();
        }

        InstanceScript* instance;

        void Reset() 
		{
			events.Reset();
		}

        void EnterCombat(Unit* /*who*/) 
		{
			events.ScheduleEvent(EVENT_BERSERKER, 10000);
			events.ScheduleEvent(EVENT_FLAMEBREAKER, 13000);
			events.ScheduleEvent(EVENT_ROAR, 18000);
			events.ScheduleEvent(EVENT_MAGMASPIT, 15000);
		}

        void UpdateAI(const uint32 Diff)
        {
            if (!UpdateVictim())
                return;

			events.Update(diff);

			while (uint32 eventId = events.ExecuteEvent())
			{
				switch (eventId)
				{

				case EVENT_BERSERKER:
					if(Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0, 200, true))
					DoCast(pTarget,SPELL_BERSERKER_CHARGE);
					events.ScheduleEvent(EVENT_BERSERKER, 10000);
					break;

				case EVENT_FLAMEBREAKER:
					DoCastAOE(SPELL_FLAMEBREAK);
					events.ScheduleEvent(SPELL_FLAMEBREAK, 13000);
					break;

				case EVENT_ROAR:
					DoCastAOE(SPELL_TERRYFING_ROAR);
					events.ScheduleEvent(EVENT_TERRYFING_ROAR, 18000);
					break;

				case EVENT_MAGMASPIT
					if (Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0, 200, true))
					DoCast(DoCast(pTarget,SPELL_MAGMASPIT);
					events.ScheduleEvent(EVENT_MAGMASPIT, 15000);

				default:
					break;
				}
			}


            DoMeleeAttackIfReady();
        }
    };
};
class npc_dog_lucky : public CreatureScript
{
public:
    npc_dog_lucky() : CreatureScript("npc_dog_lucky") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_dog_luckyAI (creature);
    }

    struct npc_dog_luckyAI : public ScriptedAI
    {
        npc_dog_luckyAI(Creature* creature) : ScriptedAI(creature), Intialized(false) {}

		
		EventMap events;

		void Reset()
		{
			events.Reset();
			me->SetReactState(REACT_PASSIVE);
		}

		void EnterCombat(Unit* /*who*/) 
		{
			events.ScheduleEvent(EVENT_BREATH, 15000);
			events.ScheduleEvent(EVENT_LAVA, 30000);
		}

        void UpdateAI(const uint32 Diff)
        {
			events.Update(diff);

			while (uint32 eventId = events.ExecuteEvent())
			{
				switch (eventId)
				{
			case EVENT_LAVA:
				if (Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0, 200, true))
					DoCast(DoCast(pTarget,SPELL_LAVA_DROOL);
					events.ScheduleEvent(EVENT_LAVA, 30000);
			case EVENT_BREATH:
					DoCastAOE(SPELL_FLAME_BREATH);
					events.ScheduleEvent(EVENT_BREATH, 15000);
					default:
					break;
				}
					
		}

            DoMeleeAttackIfReady();
        }
    };

class npc_dog_buster : public CreatureScript
{
public:
    npc_dog_buster() : CreatureScript("npc_dog_buster") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_dog_busterAI (creature);
    }

    struct npc_dog_busterAI : public ScriptedAI
    {
        npc_dog_busterAI(Creature* creature) : ScriptedAI(creature), Intialized(false) {}

		
			
		EventMap events;

		void Reset()
		{
			events.Reset();
			me->SetReactState(REACT_PASSIVE);
		}

		void EnterCombat(Unit* /*who*/) 
		{
			events.ScheduleEvent(EVENT_BREATH, 15000);
			events.ScheduleEvent(EVENT_LAVA, 30000);
		}

        void UpdateAI(const uint32 Diff)
        {
			events.Update(diff);

			while (uint32 eventId = events.ExecuteEvent())
			{
				switch (eventId)
				{
			case EVENT_LAVA:
				if (Unit* pTarget = SelectTarget(SELECT_TARGET_RANDOM, 0, 200, true))
					DoCast(DoCast(pTarget,SPELL_LAVA_DROOL);
					events.ScheduleEvent(EVENT_LAVA, 30000);
			case EVENT_BREATH:
					DoCastAOE(SPELL_FLAME_BREATH);
					events.ScheduleEvent(EVENT_BREATH, 15000);
					default:
					break;
				}
					
		}

            DoMeleeAttackIfReady();
        }
    };

class npc_dog_spot : public CreatureScript
{
public:
    npc_dog_spot() : CreatureScript("npc_dog_spot") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new npc_dog_spotAI (creature);
    }

    struct npc_dog_spotAI : public ScriptedAI
    {
        npc_dog_spotAI(Creature* creature) : ScriptedAI(creature), Intialized(false) {}

		
		EventMap events;

		void Reset()
		{
			events.Reset();
			
			me->SetReactState(REACT_PASSIVE);
		}

        void UpdateAI(const uint32 Diff)
        {
			//� rev�rifier
					
		}

            DoMeleeAttackIfReady();
        }
    };

void AddSC_boss_beauty()
{
	new npc_dog_lucky();
	new npc_dog_spot();
	new npc_dog_buster();
    new boss_beauty();
}