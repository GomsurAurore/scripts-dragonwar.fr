/*
	Script fait pour le serveur priv� DragonWar.fr

	�tat du script : 5%
	Avancement : Reste � script le boss --> Poteau :/

	N.B.: Ne d�tient aucun copyright, peut �tre modifi� sans aucune permission de l'auteur.
*/


#include "ScriptPCH.h"
#include "blackrock_caverns.h"

enum Spells
{
	SPELL_QUICKSILVER_ARMOR = 75842,
	SPELL_QUICKSILVER_HEATED = 75846,
	SPELL_HEATWAVE = 63677,
	SPELL_BURNING_METAL = 76002,
	SPELL_CHAIN = 845,
	SPELL_LAVA_SPOUT = 76007,
}

enum Events
{
	EVENT_HEATWAVE = 1,
	EVENT_BURNING_METAL = 2,
	EVENT_CHAIN = 3,
	EVENT_LAVA = 4,
}


class boss_karsh_steelbender : public CreatureScript
{
public:
    boss_karsh_steelbender() : CreatureScript("boss_karsh_steelbender") { }

    CreatureAI* GetAI(Creature* creature) const
    {
        return new boss_karsh_steelbenderAI (creature);
    }

    struct boss_karsh_steelbenderAI : public ScriptedAI
    {
        boss_karsh_steelbenderAI(Creature* creature) : ScriptedAI(creature)
        {
            instance = creature->GetInstanceScript();
        }

        InstanceScript* instance;

        void Reset() {}

        void EnterCombat(Unit* /*who*/) {}

        void UpdateAI(const uint32 Diff)
        {
            if (!UpdateVictim())
                return;

            DoMeleeAttackIfReady();
        }
    };
};

void AddSC_boss_karsh_steelbender()
{
    new boss_karsh_steelbender();
}